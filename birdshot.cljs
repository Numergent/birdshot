#!/usr/bin/env lumo
(ns birdshot.core
  (:require
   [cljs.core :refer [*command-line-args*]]
   [cljs.reader :as edn]
   [clojure.string :as string]
   [clojure.walk :refer [keywordize-keys]]
   ["fs" :as fs]
   ["twitter" :as twitter]
   ["moment" :as moment]
   ["csv-parse/lib/sync" :as csv]))

;;;;;;;;;;;;;;;;;;
;;; Constants
;;;;;;;;;;;;;;;;;;

(def status-not-found 144)

;;;;;;;;;;;;;;
;;; Helpers
;;;;;;;;;;;;;;

(defn- obj->clj [obj]
  (reduce
    (fn [props k]
      (assoc props (keyword k) (aget obj k)))
    {}
    (js/Object.keys obj)))

(defn js->edn [data]
  (js->clj data :keywordize-keys true))

(def now (moment.))

;;;;;;;;;;;;;;;;;;
;;; Calculations
;;;;;;;;;;;;;;;;;;

(defn age
  "Calculates the age of an item."
  [item]
  ;; We have two possible values for calculating the age. An archive has a timestamp
  ;; column, whereas the timeline returns a created_at date as a string.
  (if item (.diff now (moment. (or (:timestamp item) (js/Date. (:created_at item)))) "days") -1))

(defn convert-twitter-item
  "Converts an archive row into a twitter item we can process"
  [item]
  (-> item
      obj->clj
      (update :timestamp #(js/Date. %1))))

(defn should-delete [tweet max-age]
  (> (age tweet) max-age))

(defn tweets-older-than
  [archive max-age]
  (filter #(should-delete % max-age) archive))

(defn tweets-to-delete
  "Returns a list of tweets to delete in reverse order, so that we can kill
   the oldest ones first"
  [archive max-age]
  (-> archive
      (tweets-older-than max-age)
      reverse))

(defn load-archive
  [filename]
  (when filename
    (map convert-twitter-item 
         (-> filename
             fs/readFileSync 
             str 
             (csv #js {:delimiter "," :columns true})))))

(defn parse-arguments 
  []
  (try 
    (->> *command-line-args*
         (map #(string/split %1 "="))
         (into {})
         keywordize-keys)
    (catch :default e (throw (js/Error "ERROR: Arguments are expected in pairs as `arg=value`.")))))

(defn find-config [arguments]
  (or (:config arguments)
      (-> js/process .-env .-BIRDSHOT_CONFIG)
      "config.edn"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Configuration and settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def default-max-age 365)
(def arguments (parse-arguments)) 
(def max-age (let [parsed (js/parseInt (:max-age arguments))]
                (if (js/isNaN parsed) default-max-age parsed)))

(def config (find-config arguments))
(println "Using config file" config)
(def config (-> config fs/readFileSync str edn/read-string))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Delete from archive
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def archive (when-let [archive-name (:archive arguments)]
               (load-archive archive-name)))

;; Tweets to delete from the archive
(def to-delete (tweets-to-delete archive max-age))

(when archive 
  (println "We have a tweet archive. Deleting old tweets from it first.")
  (println "- Oldest tweet in archive is" (age (last archive)) "days old.")
  (println "- Found" (count to-delete) "tweets to delete on the archive."))

(defn delete-tweet 
  "Deletes a tweet id using an existing twitter client"
  [client sid]
  (when client
    (.post client
           (str "statuses/destroy/" sid)
           ;; Only report errors when it's not a "status not found"
           #(when-let [err (first (js->clj %))]
               (when (not= status-not-found (get err "code"))
                 (println "Possible error deleting" sid err))))))

;; Twitter client and configuration values
(defn new-client []
  (some-> config :twitter :access-keys clj->js twitter.))
(def account (some-> config :twitter :access-keys :account))


;; Deletes old tweets from the archive
;;
;; I could accumulate the total tweets that weren't found, but the call to
;; twitter-client is async. Not worth the coordination for now.
(def twitter-client (new-client))
(doseq [item to-delete]
  (delete-tweet twitter-client (:tweet_id item)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Query and purge timeline
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(println "Walking timeline.")


(def status-params {:screen_name account :exclude_replies false :include_rts true :count 400})

(declare get-timeline-to-process)

(defn process-and-callback 
  [raw-timeline]
  (let [timeline   (js->edn raw-timeline)
        next-start (last timeline)
        age-last   (age next-start)
        max-id     (:id next-start)
        ;; Let's not assume that the client is stateless and not used anywhere else
        client     (new-client)]
    (doseq [tweet timeline]
        (when (should-delete tweet max-age)
          (println "Deleting" (:id tweet) "Age:" (age tweet))
          (delete-tweet client (:id_str tweet))))
    ;; If we've reached the last element of our query, querying for the last
    ;; ID will still return the same element. This will recur only when we
    ;; have more than one element in the list.
    ;;
    ;; Yes, I'd have imagined that if we delete the last element then we won't
    ;; get it again, but calls don't necessarily take effect immediately.
    (when (and max-id 
               (< 1 (count timeline)))
      (println "Re-querying from id" max-id "aged" age-last)
      (get-timeline-to-process (assoc status-params :max_id max-id)))))

(defn process-timeline 
  [error timeline]
  (if error
    (println error)
    (process-and-callback timeline)))

(defn get-timeline-to-process 
  [params]
  (.get twitter-client
      "statuses/user_timeline"
      (clj->js params)
      process-timeline))

(get-timeline-to-process status-params)