# Birdshot

## Description

Bot for periodically removing old Tweets. Inspired by Vicki Lai's post on [ephemeral tweets](https://vickylai.com/verbose/delete-old-tweets-ephemeral/).

**This is not a drill. Running this will remove old tweets without any further confirmation.**

## Installation

1. Make sure you have [`lumo`](https://github.com/anmonteiro/lumo/wiki/Install) installed
2. install [Node.js](https://nodejs.org/en/)
3. run `npm install` to install Node modules

You could then run `npm start` to run Birdshot with the default parameters, but chances are you need to configure it first.

## Usage

Create a file called `config.edn` with the following contents:

```clojure
{;; add Twitter config for your account
 :twitter {:access-keys
           {:consumer_key "XXXX"
            :consumer_secret "XXXX"
            :access_token_key "XXXX"
            :access_token_secret "XXXX"}
           :account "your-account"}}
```

The bot looks for `config.edn` at its relative path by default. You can specify an alternative location either using the `BIRDSHOT_CONFIG` environment variable or passing the path to the file as an argument.

You can run the bot with `./birdshot.cljs`.

To periodically clean up, use a cron job such as:

    */30 * * * * birdshot.cljs config=/path/to/config.edn > /dev/null 2>&1

## Limitations

Twitter's status API can only return the last 3200 items. If you have more than 3200 tweets on your timeline, you'll need to retrieve your twitter archive so that birdshot can use it to retrieve the IDs to delete.

This version works only based off the Twitter archive (as I have a _lot_ of tweets to delete myself).

## Valid command line arguments

* `config` indicates the edn config file to load the twitter set up from.
* `archive` points to the Twitter archive file, which may be necessary to clean up your oldest Tweets. If you don't provide one, Birdshot will only operate [on the Tweets it can retrieve from the user's timeline](https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline.html) (the last 3200).
* `max-age` indicates the maximum number of days a Tweet is allowed to live. The default is 365 days. Any tweet older than that is deleted.

As an example, to clean up any tweet older than 15 days, run `./birdshot.cljs archive=tweets.csv max-age=15`.

## TODO

- [x] Retrieve existing tweets via the status list, walk backwards. (20180819)
- [ ] Expand to consider "Likes" as well.
- [ ] Add a changelog.

## License

Copyright © 2018 Numergent Limited. Loosely based off Dmitri Sotnikov's [mastodon-bot](https://github.com/yogthos/mastodon-bot).

Distributed under the [MIT License](http://opensource.org/licenses/MIT).
